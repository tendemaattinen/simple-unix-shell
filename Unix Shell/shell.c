/**********************************
* Author: Teemu Tynkkynen
* Student ID: 0418815
* LUT
********************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#define LOGOUT 15
#define MAXNUM 40
#define MAXLEN 160

void sighandler(int sig) {
	switch (sig) {
		case SIGALRM:
			printf("\nautologout\n");
			exit(0);
		default:
			break;
	}
	return;
}

/* Main function */
int main() {

	char * cmd, str[MAXLEN], * args[MAXNUM], * args1[MAXNUM], * args2[MAXNUM];
	int background, i, pid1, selector, pid2, pipesel, c, len1, len2, fd0, fd1, a, lenght, b;
	char cwd[1024], home[1024];
	int pipes[2];
	pipe(pipes);
	chdir(getenv("HOME"));

    signal(SIGALRM, sighandler);
	signal(SIGINT, sighandler);

	/* Loop for shell */
    while(1) {
		
		selector = 0;
        background = 0;
		len1 = 0;
		len2 = 0;
		
		/* Create pipes */
		int pipes[2];
		pipe(pipes);
		pipesel = 0;
		
		/* Get current directory using getcwd() */
		getcwd(cwd, sizeof(cwd));
		
		/* Print current directory */
        printf("$ %s > ", cwd);
        fgets(str, MAXLEN, stdin);
        str[strlen(str) - 1] = '\0';
        
        if (str[strlen(str)-1] == '&') {
			str[strlen(str)-1]=0;
			background = 1;
		}

		if (strlen(str) == 0)
			continue;

		/* Parse users input and save arguments to array*/
        cmd = str;
        i = 0;
		lenght = 0;
		while ( (args[i] = strtok(cmd, " ")) != NULL) {
			i++;
			lenght++;
			cmd = NULL;
		}

		/* Check if users input is exit */
        if (strcmp(args[0],"exit")==0) {
			exit(0);
		}

		/* Select right selector if line includes <, >, >> or | */
		for (a = 0; a < lenght; a++) {
			/* Try to find < */
			if (strcmp(args[a], "<") == 0) {
				if ((lenght - a) > 2) {
					/* Try to find > */
					if (strcmp(args[a + 2], ">") == 0){
						args[a + 2] = NULL;
						args[a] = NULL;
						selector = 4;
					} else {
						args[a] = NULL;
						selector = 3;
					}
				} else {
					args[a] = NULL;
					selector = 3;
				}
				break;
				/* Try to find > */
			} if (strcmp(args[a], ">") == 0) {
				args[a] = NULL;
				selector = 1;
				break;
				/* Try to find >> */
			} if (strcmp(args[a], ">>") == 0) {
				args[a] = NULL;
				selector = 2;
				break;
				/* Try to find | */
			} if (strcmp(args[a], "|") == 0) {
				c = a + 1;
				for (b = 0; b < a; b++) {
					args1[b] = args[b];
					len1 = len1 + 1;
				} 
				for (b = 0; b < lenght - (c); b++) {
					args2[b] = args[c];
					len2 = len2 + 1;
					c = c + 1;
				}
				args[a] = NULL;
				args1[len1] = NULL;
				args2[len2] = NULL;
				selector = 5;
				pipesel = 1;
			}
		}

		/* Change current directory */
		if (strcmp(args[0], "cd") == 0) {
			if (args[1] == NULL) {
				chdir(getenv("HOME"));
			} else {
				if (chdir(args[1]) != 0) {
					perror("chdir");
				} else {
					getcwd(cwd, sizeof(cwd));
				}
			}
		}
		
        switch (pid1 = fork()) {
			case -1:
				/* Give fork error */
				perror("fork");
				continue;
			case 0:
				/* Child process */
				if (strcmp(args[0], "cd") == 0) {	
					exit(1);
				} else if (selector == 1){
					fd1 = open(args[lenght - 1],O_WRONLY|O_CREAT,1234);       

        			dup2(fd1, STDOUT_FILENO);
        			close(fd1);

					execvp(args[0], args);
				   	perror("execvp");
				    exit(1);
				
				} else if (selector == 2){
        			fd1 = open(args[lenght - 1],O_WRONLY|O_APPEND,1234);       

        			dup2(fd1, STDOUT_FILENO);
        			close(fd1);

					execvp(args[0], args);
				   	perror("execvp");
				    exit(1);

				} else if (selector == 3){
					fd0 = open(args[lenght - 1], O_RDONLY);
					dup2(fd0,STDIN_FILENO);
					close(fd0);

					execvp(args[0], args);
				   	perror("execvp");
				    exit(1);

				} else if (selector == 4){
					fd1 = open(args[lenght - 1],O_WRONLY|O_CREAT,1234);       

        			dup2(fd1, STDOUT_FILENO);
        			close(fd1);

					fd0 = open(args[lenght - 3], O_RDONLY);

					dup2(fd0,STDIN_FILENO);
					close(fd0);

					execvp(args[0], args);
				   	perror("execvp");
				    exit(1);
				} else if (selector == 5){
					dup2(pipes[1], STDOUT_FILENO);
					close(pipes[0]);
					execvp(args1[0], args1);
					perror("execvp");
					exit(1);
				} else {
				    execvp(args[0], args);
				    perror("execvp");
				    exit(1);
                }
		}
		if (pipesel == 1) {
			switch (pid2 = fork()) {
				case -1:
					/* Give fork error */
					perror("fork");
					continue;
				case 0:
					dup2(pipes[0], STDIN_FILENO);
					close(pipes[1]);
					execvp(args2[0], args2);
					perror("execvp");
					exit(1);
			}
		}
		/* Close pipes */
		close(pipes[0]);
		close(pipes[1]);
		waitpid(pid1);
		if (pipesel == 1) {
			waitpid(pid2);
		}
    }                          
   return 0;
}